const AFFINITY = 'affinity';
const COMMUNICATION = 'commucation';
const IMPORTANCE = 'importance';
const CIRCLE_MIN_R = 3;
const PERSON_CIRCLE_R = 10;
const PERSON_COLOR = '#000';
const PERSON_TEXT_COLOR = '#fff';
const PERSON_TEXT_BG = PERSON_COLOR;
const PERSON_TEXT_PADDING = 10;
const CONTACT_TEXT_COLOR = '#000';
const WIDTH = 960;
const HEIGHT = 960;

// scale color to importance
const color = d3.scale.linear()
    .domain([0, 100])
    .range(["#b8eaed", "#083144"]);

// scale nodes distance to affinity
const distance = d3.scale.linear()
    .domain([0, 100])
    .range([200, 20]);

d3.json("data.json", function (error, person) {

    // let the person be the first node
    const nodes = {
        0: {
            name: person.name
        }
    };

    // link contacts to the first node
    const links = person.contacts.map((contact, i) => (
    {
        source: nodes[0],
        target: (nodes[i + 1] = {
            name: contact.name,
            [IMPORTANCE]: contact[IMPORTANCE],
            [COMMUNICATION]: contact[COMMUNICATION],
        }),
        distance: distance(contact[AFFINITY])
    }
    ))

    // sets the layout
    const force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(links)
        .size([WIDTH, HEIGHT])
        .linkDistance(d => d.distance)
        .charge((d, i) => -10000)
        .on("tick", tick)
        .start();

    // sets the svg
    const svg = d3.select("body")
        .attr("width", WIDTH + "px")
        .attr("height", HEIGHT + "px")
        .append("svg")
        .attr("width", WIDTH + "px")
        .attr("height", HEIGHT + "px");

    // sets the node links
    const path = svg
        .append("svg:g")
        .selectAll("path")
        .data(links)
        .enter()
        .append("svg:path")
        .attr("class", "link")

    // sets the nodes
    const node = svg.selectAll(".node")
        .data(d3.values(nodes))
        .enter()
        .append("g")

    node.append("circle")
        .attr("r", getCircleSize)
        .style("fill", getCircleColor);

    // add text background to the first node
    node.each(function (d, i) {
        let g = d3.select(this);
        if (i === 0) {
            svg.append("text")
                .text(person.name)
                .each(function () {
                    const width = this.getBBox().width;
                    g.insert("rect")
                        .attr("x", ((width / 2) * -1) - PERSON_TEXT_PADDING)
                        .attr("y", -32)
                        .attr("width", width + PERSON_TEXT_PADDING * 2)
                        .attr("height", 20)
                        .attr("rx", 10)
                        .attr("ry", 10)
                        .style("fill", PERSON_TEXT_BG);
                })
                .remove()
        }
    })

    // position labels above circles
    node.append("text")
        .attr("y", (d, i) => (getCircleSize(d, i) * -1) - 7)
        .attr("text-anchor", "middle")
        .text(d => d.name)
        .style("fill", (d, i) => {
            if (i === 0) {
                return PERSON_TEXT_COLOR;
            }
            return CONTACT_TEXT_COLOR
        })

    // make the curvy lines
    function tick() {
        path.attr("d", function (d) {
            const dx = d.target.x - d.source.x,
                dy = d.target.y - d.source.y,
                dr = Math.sqrt(dx * dx + dy * dy);
            return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
        });
        node.attr("transform", d => "translate(" + d.x + "," + d.y + ")")
            .attr("cx", function (d, i) {
                if (i === 0) return d.x = WIDTH / 2; // place person at center
                return d.x = Math.max(100, Math.min(WIDTH - 100, d.x));
            })
            .attr("cy", function (d, i) {
                if (i === 0) return d.y = HEIGHT / 2; // place person at center
                return d.y = Math.max(100, Math.min(HEIGHT - 100, d.y));
            });
    }

    function getCircleSize(d, i) {
        if (i === 0) {
            return PERSON_CIRCLE_R;
        }
        return Math.floor(d[COMMUNICATION] / 10) * 5 || CIRCLE_MIN_R
    }

    function getCircleColor(d, i) {
        if (i === 0) {
            return PERSON_COLOR;
        }
        return color(d[IMPORTANCE]);
    }

    // avoid animation
    while (force.alpha()) {
        force.tick()
    }
});
